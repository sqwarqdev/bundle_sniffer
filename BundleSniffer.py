#!/usr/bin/env python

# -->
import sys
import os
import argparse
import subprocess as sub
import Foundation as fnd

# string constants 
SWIFT_FRMWKS = ['libswiftAppKit.dylib', 'libswiftCore.dylib', 'libswiftCoreData.dylib', 'libswiftCoreFoundation.dylib', 'libswiftCoreGraphics.dylib', 'libswiftCoreImage.dylib', 'libswiftDarwin.dylib', 'libswiftDispatch.dylib', 'libswiftFoundation.dylib', 'libswiftIOKit.dylib', 'libswiftMetal.dylib', 'libswiftObjectiveC.dylib', 'libswiftos.dylib', 'libswiftQuartzCore.dylib', 'libswiftXPC.dylib']
USER_MSG_INTEREST = "Strings of interest found in Mach-O binary:"
USER_MSG_CNT = "/Contents:"
USER_MSG_START = "Sniffing bundle..."
USER_MSG_SWIFT = "Swift (Apple)"
USER_MSG_HELP = "Specify the path to the bundle you want to sniff"
CONTENTS = "/Contents"
DSKTP = "Desktop"
LNCH = "Launch"
LIBR = "Library"
APPSUPP = "Application Support"
DWNLDS = "Downloads"
FWKS = "Frameworks"
INFO_P = "Info.plist"
MAC_OS = "MacOS"
RSRCS = "Resources"

# -->
class cd:
    """ Context manager for changing the current working directory """
    # --
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

	# --
    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

	# --
    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


# -->
class BundleSniffer(object):
	""" Primary workhorse """

	# --
	def __init__(self, bundlePath):
		self.bundlePath = bundlePath
		self.contents = bundlePath + CONTENTS
		
	
	# --
	def localDir(self, dir):
		""" function for getting current user paths """
		user = fnd.NSUserName()
		homeDir = fnd.NSHomeDirectoryForUser(user)
		desktopDir = homeDir + "/" + DSKTP
		userLibDir = homeDir + "/" + LIBR
		userAppSuppDir = userLibDir + "/" + APPSUPP

		downloadsDir = homeDir + "/" + DWNLDS
		if dir == "home" or dir == "~":
			return homeDir
		elif dir == DSKTP:
			return desktopDir
		elif dir == LIBR:
			return userLibDir
		elif dir == APPSUPP:
			return userAppSuppDir
		elif dir == DWNLDS:
			return downloadsDir
		else:
			return homeDir + "/" + dir
			

	# --
	def bidFromPlist(self, aPath):
		output = []
		stoutpl = self.localDir(DSKTP + "/stoutpl")
		infoplist = aPath + "/" + INFO_P
		target = "Error: Not found"
		sub.call(['plutil', '-p', infoplist], stdout=open(stoutpl, 'w'))
		fin = open(stoutpl, "r")
		g = fin.read()
		sep = "=>"
		linesplit = "\n"
		bidTag = "CFBundleIdentifier"
		verTag = "CFBundleShortVersionString"
		altVerTag = "CFBundleVersionString"
		hLines = g.split(linesplit)
		for ln in hLines:
			if verTag in ln or altVerTag in ln or bidTag in ln:
				elements = ln.split(sep)
				target = elements[1]
				target = target.replace('\"', '')
				target = target.strip(' ')
				output.append(target)
				
		return output
		
				
	# --
	def getPathStringsFromBinary(self, aPath):
		try:
			stout = self.localDir(DSKTP + "/stout")
		except:
			print("\nError: stout unavailable.")
		else:
			macos = aPath + "/" + MAC_OS
			
		try:
			filelist = os.listdir(macos)
		except:
			print("\nError: no such file as " + macos)
			quit()
		else:
			for f in filelist:
				binpath = macos + "/" + f
				binpath = binpath.replace(' ', '\\ ')
				isBinary = os.system("file -b " + binpath + " | grep Mach-O > /dev/null")
				if isBinary == 0:
					binpath = binpath.replace('\\ ', ' ')
					linenum_h = 0
					linenum_l = 0
					linenum_ln = 0
					linenum_p = 0
					https = "http"
					lib = LIBR
					lnch = LNCH
					httplist = [] 
					liblist = []
					lnchlist = []
					pathdivlist = []
					sub.call(['strings', '-a', binpath], stdout=open(stout, 'w'))
					for ln in open(stout, "r"):
						if https in ln or lib in ln or lnch in ln or "/" in ln:
							if len(ln) > 1:
								if https in ln:
									linenum_h += 1
									httplist.append("\t%s : %s" % (linenum_h, ln))
								elif lib in ln:
									linenum_l += 1
									liblist.append("\t%s : %s" % (linenum_l, ln))
								elif lnch in ln:
									linenum_ln += 1
									lnchlist.append("\t%s : %s" % (linenum_ln, ln))
								else:
									linenum_p += 1
									pathdivlist.append("\t%s : %s" % (linenum_p, ln))
									
					
					print "\nHTTP:\n"
					for h in httplist:
						print h
					
					print "\nLIBRARY:\n"
					for l in liblist:
						print l
						
					print "\nLAUNCH:\n"
					for lc in lnchlist:
						print lc	
						
					print "\nOTHER /..:\n"	
					for p in pathdivlist:
						print p		
		
		

							
	# --
	def listContents(self, aPath):
		try:
			filelist = os.listdir(aPath)
		except:
			print ("\nError: no such file as " + aPath)
			quit()
		else:
			for f in filelist:
				print(" \t %s" % f)
				subpath = aPath + "/" + f
				if os.path.isdir(subpath):
					if FWKS in f or "PlugIns" in f or "MacOS" in f:
						externalFmwks = []
						frs = os.listdir(subpath)
						for fr in frs:
							childpath = subpath + "/" + fr
							if fr not in SWIFT_FRMWKS:
								if os.path.isdir(childpath):
									externalFmwks.append(fr + "/")
								else:
									childpath = childpath.replace(' ', '\ ')
									isBinary = os.system("file -b " + childpath + " | grep Mach-O > /dev/null")
									if isBinary == 0:
										externalFmwks.append(fr + " +x")
							else:
								s = USER_MSG_SWIFT
								if s not in externalFmwks:
									externalFmwks.append(s)
					
						print("\t\t %s" % externalFmwks)
					elif RSRCS in f:
						rsrs = os.listdir(subpath)
						resources = []
						for r in rsrs:
							childpath = subpath + "/" + r
							if os.path.isdir(childpath):
								resources.append(r + "/")
							else:
								childpath = childpath.replace(' ', '\ ')
								isBinary = os.system("file -b " + childpath + " | grep Mach-O > /dev/null")
								if isBinary == 0:
									resources.append(r + " +x")
							
						print("\t\t %s" % resources)
				
				elif INFO_P in f:
							bid = self.bidFromPlist(self.contents)
							print("\t\t %s" % bid)

		
	# --
	def run(self):
		print("\n %s" % USER_MSG_START)
		print("\n %s" % USER_MSG_CNT)
		self.listContents(self.contents)
		print("\n\n\n %s \n" % USER_MSG_INTEREST)
		self.getPathStringsFromBinary(self.contents)
		

# -->
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-b", "--bundle", required=True, help=USER_MSG_HELP)
	args = parser.parse_args()
	bundleSniffer = BundleSniffer(args.bundle)
	bundleSniffer.run()
