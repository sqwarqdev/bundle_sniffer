# README


### OBJECTIVE:

BundleSniffer makes a static inspection of the contents of macOS bundles and 'sniffs' them for indicators of malware.

It is intended to be a complimentary tool for use with other inspection tools like RBAppCheckerLite, Pacifist, Suspicious Package and similar. 



### STATUS:

Still very much in the earliest stage of development, and not much more than a 'toy' at the moment.



### NOTES:


#### Usage:
Use the -b flag and specify a path to an app:

>`./BundleSniffer.py -b /Applications/Target.app`

#### Output:

	
		Sniffing bundle...
		/Applications/Target.app/Contents

		 /Contents:
			 _CodeSignature
			 Frameworks
				 ['UpdateKit.framework/']
			 Info.plist
				 ['com.target.app', '1.1.2']
			 MacOS
				 ['Target +x']
			 PkgInfo
			 PlugIns
				 ['Language Modules/']
			 Resources
				 ['English.lproj/', 'aschng +x', 'TargetWindow.nib/']


			 Strings of interest found in Mach-O binary: Target
			 
			 HTTP:

					1 : http://www.twitter.com/target

					2 : http://svn.aws.link/pw.html
			LAUNCH:

					1 : applicationWillFinishLaunching:

					2 : applicationDidFinishLaunching:

					3 : setLaunchPath:


			OTHER /..:

					1 : /tmp/.morris

					2 : /usr/bin/git

					3 : /private/var/tmp/.rslog
					
					4 : file://localhost/%@


#### Behaviour:

i. Lists the contents of /../Contents
	- if 'Contents' does not exist, throws an error and quits
	
ii. Lists the contents of Frameworks. Standard Apple Swift frameworks are just listed once as 'Swift (Apple)'

iii. Info.plist 
	- extracts, if present, the CFBundleIdentifier and CFBundleShortVersionString, or if missing, CFBundleVersionString.
	Note that here and elsewhere (e.g, Resources, binary files are denoted by appending '+x';)
	
i. Lists all  binaries in 'MacOS' and alter examines each ones strings.

v. Lists the contents of PlugIns.

vi. Lists the contents of Resources
	- Only lists folders and binary files; is not recursive.

vii. Strings of interest

   -- Lists strings extracted from binaries in MacOS:  
+ HTTP - any string with 'http' in it   
+ LAUNCH - any string not already listed in HTTP that contains 'Launch'   
+ OTHER /.. - any string not already listed in HTTP or LAUNCH with a path divider and that's more than 1 char in length   



	
	



	


